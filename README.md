# javascript-monorepo

When there is a need for an updated version of the Jint.dll - ensure you grab the Jint.dll from the netstandard2.1 directory.

## Trouble-Shooting

1.
If you get the following exception:

```
System.TypeLoadException : Could not load type 'System.Runtime.CompilerServices.Closure' from assembly 'System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.
Stack trace:
   at Jint.Runtime.Interop.DelegateWrapper.Call(JsValue thisObject, JsValue[] jsArguments)
   at Jint.Engine.Call(FunctionInstance functionInstance, JsValue thisObject, JsValue[] arguments, JintExpression expression)
   at Jint.Engine.Call(ICallable callable, JsValue thisObject, JsValue[] arguments, JintExpression expression)
   at Jint.Runtime.Interpreter.Expressions.JintCallExpression.EvaluateCall(EvaluationContext context, JsValue func, Object reference, NodeList`1& arguments, Boolean tailPosition)
   at Jint.Runtime.Interpreter.Expressions.JintCallExpression.Call(EvaluationContext context)
   at Jint.Runtime.Interpreter.Expressions.JintCallExpression.EvaluateInternal(EvaluationContext context)
   at Jint.Runtime.Interpreter.Statements.JintExpressionStatement.ExecuteInternal(EvaluationContext context)
   at Jint.Runtime.Interpreter.JintStatementList.Execute(EvaluationContext context)
   at Jint.Runtime.Interpreter.Statements.JintBlockStatement.ExecuteInternal(EvaluationContext context)
   at Jint.Runtime.Interpreter.JintStatementList.Execute(EvaluationContext context)
   at Jint.Engine.<>c__DisplayClass68_0.<Execute>g__DoInvoke|0()
   at Jint.Engine.ExecuteWithConstraints[T](Boolean strict, Func`1 callback)
   at Jint.Engine.Execute(Script script)
   at Jint.Engine.Evaluate(Script script)
   at Jint.Engine.Evaluate(String source, ParserOptions parserOptions)
   at Jint.Engine.Evaluate(String source)
   at Javascript.Jint.JintVirtualMachine._inMemModuleLoadingEngine_ModuleRequested(Object sender, ModuleRequestedEventArgs e) in /Users/tcassidy/gitlab/javascript-monorepo/javascript-jint/Runtime/dfoss-javascript-jint/jint/JintVirtualMachine.cs:line 171
   at Jint.CommonJS.ModuleLoadingEngine.Load(String moduleName, Module parent) in /Users/tcassidy/gitlab/javascript-monorepo/javascript-jint/Runtime/dfoss-javascript-jint/Jint.CommonJS/ModuleLoadingEngine.cs:line 164
   at Jint.CommonJS.ModuleLoadingEngine.RunMain(String mainModuleName) in /Users/tcassidy/gitlab/javascript-monorepo/javascript-jint/Runtime/dfoss-javascript-jint/Jint.CommonJS/ModuleLoadingEngine.cs:line 141
   at Javascript.Jint.JintVirtualMachine.ExecuteInMem(String moduleName) in /Users/tcassidy/gitlab/javascript-monorepo/javascript-jint/Runtime/dfoss-javascript-jint/jint/JintVirtualMachine.cs:line 213
   at Javascript.Jint.JintVirtualMachine.EvalInMem(String filename, String code) in /Users/tcassidy/gitlab/javascript-monorepo/javascript-jint/Runtime/dfoss-javascript-jint/jint/JintVirtualMachine.cs:line 245
   at JintObservableTests.JavascriptThrowInSetTimeout() in /Users/tcassidy/gitlab/javascript-monorepo/javascript-jint/Runtime/dfoss-javascript-jint-test/tests/JintObservableTests.cs:line 96
```

It means the Jint.dll is using a different target version of .NET than your project.

The jint package produces the following bin directories:
- net4xx (where xx might be "61" or "8" or whatever minor/bug version of .NET)
- netstandard2.0
- netstandard2.1

You want the Jint.dll from the netstandard2.1 directory.


2.
If you don't have the a recent enough version of .NET:

```
/Users/tcassidy/gitlab/javascript-monorepo/javascript-generate-typescript-declarations/Editor/dfoss-js-generate-ts-declarations/Codegen/Typescript/CodegenType.cs(37,37): Error CS0246: The type or namespace name 'ValueTuple<,,,>' could not be found (are you missing a using directive or an assembly reference?) (CS0246) (dfoss-js-generate-ts-declarations)
```
