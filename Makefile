clean-javascript-core:
	rm -rf javascript-core/Runtime/dfoss-javascript-core/bin*
	rm -rf javascript-core/Runtime/dfoss-javascript-core/obj*
	rm -rf javascript-core/Runtime/dfoss-javascript-core/.out

clean-javascript-jint:
	rm -rf javascript-jint/Runtime/dfoss-javascript-jint/bin*
	rm -rf javascript-jint/Runtime/dfoss-javascript-jint/obj*
	rm -rf javascript-jint/Runtime/dfoss-javascript-jint/.out
	rm -rf javascript-jint/Runtime/dfoss-javascript-jint-test/bin*
	rm -rf javascript-jint/Runtime/dfoss-javascript-jint-test/obj*
	rm -rf javascript-jint/Runtime/dfoss-javascript-jint-test/.out

clean-javascript-generate-typescript-declarations:
	rm -rf javascript-generate-typescript-declarations/Editor/dfoss-js-generate-ts-declarations/bin*
	rm -rf javascript-generate-typescript-declarations/Editor/dfoss-js-generate-ts-declarations/obj*
	rm -rf javascript-generate-typescript-declarations/Editor/dfoss-js-generate-ts-declarations/.out

clean:
	make clean-javascript-core clean-javascript-jint clean-javascript-generate-typescript-declarations

test-javascript-jint:
	dotnet test javascript-jint/Runtime/dfoss-javascript-jint-test/dfoss-javascript-jint-test-monorepo.csproj

test:
	make test-javascript-jint

build-javascript-core:
	dotnet build javascript-core/Runtime/dfoss-javascript-core/dfoss-javascript-core.csproj

build-javascript-jint:
	dotnet build javascript-jint/Runtime/dfoss-javascript-jint/dfoss-javascript-jint-monorepo.csproj

build:
	make build-javascript-core build-javascript-jint

all:
	make build test
